<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div class="container">
      <spring:url value="/company/saveCompany" var="saveURL"/>
      <h2>Company</h2>
      <form:form modeAttribute="companyform" method="post" action="${saveURL}" cssClass="form">
         <form:hidden path="id"/>
         <div class ="form-group">
           <lable>Title</lable>
           <form:input path="title" cssclass="form-control"id="title"/>

      </div>
        <div class="form-group">
         <lable>Category</lable>
         <form:input path="category" cssclass="form-control"id="category"/>
     </div>
     <button type="submit" class=btn btn-primary"> save</button>
</form:form>
</body>
</html>