package com.talentbeehive.apps.repository;

import org.springframework.data.repository.CrudRepository;

import com.talentbeehive.apps.model.City;

public interface CityRepository  extends CrudRepository<City,Long>{

}
