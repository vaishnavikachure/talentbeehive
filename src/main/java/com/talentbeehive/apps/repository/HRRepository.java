package com.talentbeehive.apps.repository;

import org.springframework.data.repository.CrudRepository;

import com.talentbeehive.apps.model.HR;

public interface HRRepository extends  CrudRepository<HR, Long>{

}
