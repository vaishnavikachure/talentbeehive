package com.talentbeehive.apps.repository;

import org.springframework.data.repository.CrudRepository;

import com.talentbeehive.apps.model.Company;

public interface CompanyRepository extends CrudRepository<Company, Long> {

}
