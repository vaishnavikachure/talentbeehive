package com.talentbeehive.apps.repository;

import org.springframework.data.repository.CrudRepository;


import com.talentbeehive.apps.model.Institute;

public interface InstituteRepository extends CrudRepository<Institute, Long> {

}
