package com.talentbeehive.apps.repository;

import org.springframework.data.repository.CrudRepository;

import com.talentbeehive.apps.model.Student;

public interface StudentRepository extends CrudRepository<Student,Long> {

}
