package com.talentbeehive.apps.controller;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.talentbeehive.apps.model.HR;
import com.talentbeehive.apps.service.HRService;

@Controller
//@RequestMapping(value="/HR",method=RequestMethod.GET)

public class HRController {
	
	@Autowired
	HRService hrService;
	
	@RequestMapping("/addHr")
	public ModelAndView addHR() {
		
	    ModelAndView model=new ModelAndView();
		HR hr=new HR();
		model.addObject("HRForm",hr);
		model.setViewName("hr_form");
		return model;
	}
	//@Value("${welcome.message}")
    private String message;
    
    
    @GetMapping("/hr")
    public String main(Model model) {
        model.addAttribute("message", message);
       

        return "HR"; //view
    }
}
