package com.talentbeehive.apps.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.talentbeehive.apps.model.HR;
import com.talentbeehive.apps.model.Institute;

@Controller
//@RequestMapping(value="/Institute",method=RequestMethod.GET)
public class InstituteController {
	
	public ModelAndView addInstitute() {
		ModelAndView model=new ModelAndView();

			Institute institute= new Institute();
			model.addObject("institutefrom,institute");
			model.setViewName("institute_form");
			return model;
} 
	//@Value("${welcome.message}")
    private String message;
    
    
    @GetMapping("/institute")
    public String main(Model model) {
        model.addAttribute("message", message);
       

        return "institute"; //view
    }
}