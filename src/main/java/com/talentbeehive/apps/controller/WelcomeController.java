package com.talentbeehive.apps.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.talentbeehive.apps.model.HR;

@Controller
public class WelcomeController {

	 @Value("${welcome.message}")
	    private String message;
	    
	    
	    @GetMapping("/")
	    public String main(Model model) {
	        model.addAttribute("message", message);
	       

	        return "welcome"; //view
	    }
	    
	
}
