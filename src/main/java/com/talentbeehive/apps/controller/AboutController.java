package com.talentbeehive.apps.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AboutController {

	 @Value("${welcome.message}")
	    private String message;
	    
	    
	    @GetMapping("/about")
	    public String main(Model model) {
	        model.addAttribute("message", message);
	       

	        return "about"; //view
	    }
	    
	
}
