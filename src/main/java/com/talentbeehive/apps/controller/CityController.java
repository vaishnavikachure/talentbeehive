package com.talentbeehive.apps.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.talentbeehive.apps.model.City;

@Controller
//@RequestMapping(value ="/City", method =RequestMethod.GET)
public class CityController
{
	
	
	@RequestMapping(value="/addCity",method=RequestMethod.GET)
	public ModelAndView addCity()
	{
		ModelAndView model=new ModelAndView();
	 
		City city=new City();
		model.addObject("cityFrom",city);
		model.setViewName("city_form");
		System.out.println(model);
		return model;
	}
	//@Value("${welcome.message}")
    private String message;
    
    
    @GetMapping("/city")
    public String main(Model model) {
        model.addAttribute("message", message);
       

        return "city"; //view
    }
}