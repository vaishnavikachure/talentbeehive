package com.talentbeehive.apps.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.talentbeehive.apps.model.Student;

@Controller
//@RequestMapping(value="/Student",method=RequestMethod.GET)
public class StudentController {
	
		@RequestMapping(value="/addStudent", method=RequestMethod.GET)
		public ModelAndView addStudent() {			
			ModelAndView model=new ModelAndView ();
			Student student=new Student();
			model.addObject("studentForm",student);
			model.setViewName("student_form");
			
			return model;
		}
		//@Value("${welcome.message}")
	    private String message;
	    
	    
	    @GetMapping("/student")
	    public String main(Model model) {
	        model.addAttribute("message", message);
	       

	        return "student"; //view
	    }
	    
}
