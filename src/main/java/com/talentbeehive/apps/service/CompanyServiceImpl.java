package com.talentbeehive.apps.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.talentbeehive.apps.model.Company;
import com.talentbeehive.apps.repository.CompanyRepository;

public class CompanyServiceImpl implements CompanyService {
	
	@Autowired
	CompanyRepository companyRepository;

	@Override
	public void add(Company company) {
		companyRepository.save(company);

	}

}
