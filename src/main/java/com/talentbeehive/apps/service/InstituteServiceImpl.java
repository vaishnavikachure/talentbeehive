package com.talentbeehive.apps.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.talentbeehive.apps.model.Institute;
import com.talentbeehive.apps.repository.InstituteRepository;

public class InstituteServiceImpl implements InstituteService {
	@Autowired
	InstituteRepository instituteRepository;

	@Override
	public void add(Institute institute) {
		instituteRepository.save(institute);

	}

}
