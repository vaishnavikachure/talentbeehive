package com.talentbeehive.apps.service;


import com.talentbeehive.apps.model.Institute;

public interface InstituteService {
	public void add(Institute institute);
}
