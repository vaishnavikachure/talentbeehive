package com.talentbeehive.apps.service;

import com.talentbeehive.apps.model.Company;

public interface CompanyService {
	public void add(Company company);
}
