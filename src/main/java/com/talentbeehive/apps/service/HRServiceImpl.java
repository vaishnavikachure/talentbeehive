package com.talentbeehive.apps.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.talentbeehive.apps.model.HR;
import com.talentbeehive.apps.repository.HRRepository;

@Service
@Transactional
	
public class HRServiceImpl implements HRService {
		@Autowired
		HRRepository hrRepository ;

		@Override
		public void add(HR hr) {
			hrRepository.save(hr);
		}
	   
	}
