package com.talentbeehive.apps.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.talentbeehive.apps.model.Student;
import com.talentbeehive.apps.repository.StudentRepository;


@Service
@Transactional
public class StudentServiceImpl implements  StudentService{
	@Autowired 
	StudentRepository studentRepository;

	@Override
	public void add(Student student) {
		studentRepository.save(student);


}
}