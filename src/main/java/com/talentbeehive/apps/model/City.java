package com.talentbeehive.apps.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.stereotype.Indexed;

@Indexed
@Entity
public class City {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)

	private long city_ID;
	
	@Column(name="state")
	private String state;
	
	@Column(name="city")
	private String city;
	
	@Column(name="pincode")
	private int pincode;
	
	public City() {
		super();
		// TODO Auto-generated constructor stub
	}
	public long getCity_ID() {
		return city_ID;
	}
	public void setCity_ID(long city_ID) {
		this.city_ID = city_ID;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public int getPincode() {
		return pincode;
	}
	public void setPincode(int pincode) {
		this.pincode = pincode;
	}
	public City(long city_ID, String state, String city, int pincode) {
		super();
		this.city_ID = city_ID;
		this.state = state;
		this.city = city;
		this.pincode = pincode;
	}

}
