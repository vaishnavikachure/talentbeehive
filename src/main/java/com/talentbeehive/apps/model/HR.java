package com.talentbeehive.apps.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.stereotype.Indexed;
@Indexed
@Entity

public class HR {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int hr_ID;
	
	private String hr_FirstName;
	private String hr_LastName;
	private String company_Address;

	private int contact_No;
	

public HR() {
		super();
		// TODO Auto-generated constructor stub
	}
public String getHr_FirstName() {
	return hr_FirstName;
}
public void setHr_FirstName(String hr_FirstName) {
	this.hr_FirstName = hr_FirstName;
}
public String getHr_LastName() {
	return hr_LastName;
}
public void setHr_LastName(String hr_LastName) {
	this.hr_LastName = hr_LastName;
}
public String getCompany_Address() {
	return company_Address;
}
public void setCompany_Address(String company_Address) {
	this.company_Address = company_Address;
}
public int getHr_ID() {
	return hr_ID;
}
public void setHr_ID(int hr_ID) {
	this.hr_ID = hr_ID;
}
public int getContact_No() {
	return contact_No;
}
public void setContact_No(int contact_No) {
	this.contact_No = contact_No;
}
public HR(String hr_FirstName, String hr_LastName, String company_Address, int hr_ID, int contact_No) {
	super();
	this.hr_FirstName = hr_FirstName;
	this.hr_LastName = hr_LastName;
	this.company_Address = company_Address;
	this.hr_ID = hr_ID;
	this.contact_No = contact_No;
}


}
