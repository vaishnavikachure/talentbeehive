package com.talentbeehive.apps.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity

public class Institute {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String InstituteName;
	private String Institute_Address;
	private int Institute_ID;
	private int contact_No;
	
	
	public Institute() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getInstituteName() {
		return InstituteName;
	}
	public void setInstituteName(String instituteName) {
		InstituteName = instituteName;
	}
	public String getInstitute_Address() {
		return Institute_Address;
	}
	public void setInstitute_Address(String institute_Address) {
		Institute_Address = institute_Address;
	}
	public int getInstitute_ID() {
		return Institute_ID;
	}
	public void setInstitute_ID(int institute_ID) {
		Institute_ID = institute_ID;
	}
	public int getContact_No() {
		return contact_No;
	}
	public void setContact_No(int contact_No) {
		this.contact_No = contact_No;
	}
	
}
