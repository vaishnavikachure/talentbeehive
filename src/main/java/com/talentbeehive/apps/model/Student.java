 package com.talentbeehive.apps.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.stereotype.Indexed;
@Indexed
@Entity
public class Student {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String fist_name;
	private String last_name;
	private String address;
	private String contact_no;
	private String email_id;
    private String latest_Education;
    
    
	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	
	public String getFist_name() {
		return fist_name;
	}
	public void setFist_name(String fist_name) {
		this.fist_name = fist_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getContact_no() {
		return contact_no;
	}
	public void setContact_no(String contact_no) {
		this.contact_no = contact_no;
	}
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	public String getLatest_Education() {
		return latest_Education;
	}
	public void setLatest_Education(String latest_Education) {
		this.latest_Education = latest_Education;
	}
	public Student(long id, String fist_name, String last_name, String address, String contact_no,
			String email_id, String latest_Education) {
		super();
		this.id = id;
		
		this.fist_name = fist_name;
		this.last_name = last_name;
		this.address = address;
		this.contact_no = contact_no;
		this.email_id = email_id;
		this.latest_Education = latest_Education;
	}
    
}