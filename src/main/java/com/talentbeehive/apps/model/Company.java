package com.talentbeehive.apps.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Company {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String companyName;
	private String company_Address;
	private int company_ID;
	private int contact_No;
	
	public Company() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompany_Address() {
		return company_Address;
	}
	public void setCompany_Address(String company_Address) {
		this.company_Address = company_Address;
	}
	public int getCompany_ID() {
		return company_ID;
	}
	public void setCompany_ID(int company_ID) {
		this.company_ID = company_ID;
	}
	public int getContact_No() {
		return contact_No;
	}
	public void setContact_No(int contact_No) {
		this.contact_No = contact_No;
	}
	public Company(String companyName, String company_Address, int company_ID, int contact_No) {
		super();
		this.companyName = companyName;
		this.company_Address = company_Address;
		this.company_ID = company_ID;
		this.contact_No = contact_No;
	}

}
